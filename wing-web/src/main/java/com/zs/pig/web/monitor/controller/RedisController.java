/** Powered By 北京甜园科技, Since 2016 - 2020 */
package com.zs.pig.web.monitor.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zs.pig.common.redis.test.RedisUtils;
/**
 * 
 * @author 沈专 2016-12-8 9:27:58
 * @Email: [email]
 * @version [version]
 *	生成秘钥管理管理
 */
@Controller
@RequestMapping("redis")
public class RedisController {

	//@Resource
	private RedisUtils RedisUtils=new RedisUtils(0);
	
	@RequestMapping
	public String toGenKey(Model model){
		return "other/redis/redis";
	}
	
	
	
	/**
	 * 分页显示字典table
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public String list(int pageNum,int pageSize, Model model,String id,String sn) {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("pageNum", pageNum);params.put("pageSize", pageSize);
		Map<String, String> map =RedisUtils.hgetall("blog");
		List<MapBean> list=new ArrayList<MapBean>();
		for (Map.Entry<String, String> entry : map.entrySet()) {  
			MapBean mb=new MapBean();
			mb.setKey(entry.getKey());
			mb.setValue(entry.getValue());
			if(id!=null && id!=""){
				if(id.equals(entry.getKey())){
					list.add(mb);
				}
			}else{
				list.add(mb);
			}
			
		} 
		Page<MapBean> sublist=new Page<MapBean>();
		int total=list.size();
		if(total>0){
			sublist.setTotal(total);
			sublist.setPages((int) (total /pageSize + ((total %pageSize == 0L) ? 0
						: 1)));
			sublist.setPageNum(pageNum);sublist.setPageSize(pageSize);
			int total2=pageNum*pageSize;
			if(sublist.getPages()==pageNum){
				total2=total;
			}
			for(int i=(pageNum-1)*pageSize;i<total2;i++){
				sublist.add(list.get(i));
			}
		}
		
		
		PageHelper.startPage(params);
		//List<MapBean> sublist =list.subList((pageNum-1)*pageSize, pageNum*pageSize);
		model.addAttribute("page", new PageInfo<MapBean>(sublist));
		return "other/redis/redis-list";
	}
	/**
	 * 分页显示字典table
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "list1", method = RequestMethod.POST)
	public String list1(int pageNum,int pageSize, Model model,String id,String sn) {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("pageNum", pageNum);params.put("pageSize", pageSize);
		Map<String, String> map =RedisUtils.hgetall("00005a3c-f996-11e6-827c-204747756de9");
		List<MapBean> list=new ArrayList<MapBean>();
		for (Map.Entry<String, String> entry : map.entrySet()) {  
			MapBean mb=new MapBean();
			mb.setKey(entry.getKey());
			mb.setValue(entry.getValue());
			list.add(mb);
		} 
		Page<MapBean> sublist=new Page<MapBean>();
		int total=list.size();
		if(total>0){
			sublist.setTotal(total);
			sublist.setPages((int) (total /pageSize + ((total %pageSize == 0L) ? 0
						: 1)));
			sublist.setPageNum(pageNum);sublist.setPageSize(pageSize);
			int total2=pageNum*pageSize;
			if(sublist.getPages()==pageNum){
				total2=total;
			}
			for(int i=(pageNum-1)*pageSize;i<total2;i++){
				sublist.add(list.get(i));
			}
		}
		
		
		PageHelper.startPage(params);
		//List<MapBean> sublist =list.subList((pageNum-1)*pageSize, pageNum*pageSize);
		model.addAttribute("page", new PageInfo<MapBean>(sublist));
		return "licese/genKey/genKey-list";
	}
	public static <T> List<List<T>> split(List<T> list, int pageSize) {  
	    List<List<T>> result = new ArrayList<>(list.size() / pageSize + 1);  
	    for (int i = 0, next, max = list.size(); i < max; i = next) {  
	        next = i + pageSize;  
	        if (next > max) next = max;   
	        result.add(new ArrayList<>(list.subList(i, next)));  
	    }  
	    return result;  
	}
	
}
