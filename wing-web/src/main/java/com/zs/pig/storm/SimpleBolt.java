package com.zs.pig.storm;  
  
import java.util.Map;  
  
import backtype.storm.task.OutputCollector;  
import backtype.storm.task.TopologyContext;  
import backtype.storm.topology.BasicOutputCollector;  
import backtype.storm.topology.OutputFieldsDeclarer;  
import backtype.storm.topology.base.BaseBasicBolt;  
import backtype.storm.topology.base.BaseRichBolt;  
import backtype.storm.tuple.Fields;  
import backtype.storm.tuple.Tuple;  
import backtype.storm.tuple.Values;  
  
/** 
 * Created by IntelliJ IDEA. User: comaple.zhang Date: 12-8-28 Time: 下午2:11 To 
 * change this template use File | Settings | File Templates. 
 */  
@SuppressWarnings("serial")  
public class SimpleBolt extends BaseRichBolt {  
      
    private OutputCollector collector;  
  
    @Override  
    public void declareOutputFields(OutputFieldsDeclarer declarer) {  
        declarer.declare(new Fields("id","sn","order","type","datetime","address","user","event","params"));  
    }  
  
    @SuppressWarnings("rawtypes")  
    @Override  
    public void prepare(Map stormConf, TopologyContext context,  
            OutputCollector collector) {  
        this.collector = collector;  
    }  
  
    @Override  
    public void execute(Tuple input) {  
        try {  
            String mesg = input.getString(0);  
        //    System.out.println("msg:"+mesg);
            String [] msgs=mesg.split(" - ");
            if (mesg != null && msgs.length == 9) {  
                 collector.emit(new Values(mesg.split(" - ")[0],mesg.split(" - ")[1],mesg.split(" - ")[2],mesg.split(" - ")[3],
                 mesg.split(" - ")[4],mesg.split(" - ")[5],mesg.split(" - ")[6],mesg.split(" - ")[7],mesg.split(" - ")[8]));  
            System.out.println(mesg);  
            }  else{
            	System.out.println(mesg);
            }
        } catch (Exception e) {  
            e.printStackTrace(); // To change body of catch statement use File |  
            collector.fail(input);                      // Settings | File Templates.  
        }  
        collector.ack(input);  
    }  
}  