//Powered By zsCat, Since 2014 - 2020

package com.zs.pig.blog.serviceImpl;

import javax.annotation.Resource;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.JSONSerializableSerializer;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zs.pig.base.api.model.SysDict;
import com.zs.pig.base.api.service.SysDictService;
import com.zs.pig.blog.api.model.Blog;
import com.zs.pig.blog.api.service.BlogService;
import com.zs.pig.blog.mapper.BlogMapper;
import com.zs.pig.blog.producer.BusinessProducer;
import com.zs.pig.common.base.ServiceMybatis;
import com.zs.pig.common.utils.JSONSerializerUtil;
import com.zs.pig.common.utils.JsonUtils;

/**
* @author zsCat 2016-6-14 13:56:03
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	我的blog
 */

@Service("BlogService")
public class BlogServiceImpl extends ServiceMybatis<Blog> implements BlogService {
	static Logger logger = LogManager.getLogger(BlogServiceImpl.class.getName());
	@Resource
	private BlogMapper BlogMapper;
	@Resource
	private SysDictService sysDictService;
	@Resource
    private BusinessProducer businessProducer;
	
	/**
	 * 保存或更新
	 * 
	 * @param Blog
	 * @return
	 */
	public int saveBlog(Blog record) {
		return this.save(record);
	}

	/**
	 * 删除
	* @param CmsArticle
	* @return
	 */
	public int deleteBlog(Blog record) {
		logger.log(Level.forName("DIAG", 350), "调用blog模块  - delete方法 - "+record.getId());
		return this.delete(record);
	}

	/**
	 * 获取上一个博客
	 * @param id
	 * @return
	 */
	public Blog getLastBlog(Long id){
		logger.info("调用blog模块 - delete方法");
		return BlogMapper.getLastBlog(id);
	}
	
	/**
	 * 获取下一个博客
	 * @param id
	 * @return
	 */
	public Blog getNextBlog(Long id){
		logger.info("调用blog模块 - delete方法");
		return BlogMapper.getNextBlog(id);
	}
	public PageInfo<Blog> selectPage1(int pageNum, int pageSize, Blog record) {
		for(int i=0;i<5;i++){
			  sysDictService.select(new SysDict());
	         }
		 JSONObject ret = new JSONObject();
         ret.put("type", 1);
         ret.put("msgid", 1);
         ret.put("themeid", 1);
         ret.put("orderby", 1);
         ret.put("uid", 1);
         ret.put("username", 1);
         ret.put("isnew", 1);
         JSONObject jsonObj = new JSONObject();
         jsonObj.put("btype", "test");
         jsonObj.put("data", ret);
         try {
		//	businessProducer.sendMessageAsync(JSONSerializerUtil.serializeToBytes(jsonObj), 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
         
			PageHelper.startPage(pageNum, pageSize);
			return new PageInfo<Blog>(mapper.select(record));
		}
}
