/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package com.sankuai.xm.kafka.client.exception;

public class ProducerRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 5222623760553747260L;

	public ProducerRuntimeException() {
	}

	public ProducerRuntimeException(String message) {
		super(message);
	}

	public ProducerRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProducerRuntimeException(Throwable cause) {
		super(cause);
	}
}