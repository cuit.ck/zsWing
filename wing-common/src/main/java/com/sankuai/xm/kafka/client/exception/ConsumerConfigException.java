/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package com.sankuai.xm.kafka.client.exception;

public class ConsumerConfigException extends RuntimeException {
	private static final long serialVersionUID = 5822623760553747260L;

	public ConsumerConfigException() {
	}

	public ConsumerConfigException(String message) {
		super(message);
	}

	public ConsumerConfigException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConsumerConfigException(Throwable cause) {
		super(cause);
	}
}