/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package com.sankuai.xm.kafka.client;

public abstract interface IConsumerProcessor {
	public abstract void recvMessageWithParallel(Class paramClass,
			IMessageListener paramIMessageListener);

	public abstract void close() throws Exception;
}