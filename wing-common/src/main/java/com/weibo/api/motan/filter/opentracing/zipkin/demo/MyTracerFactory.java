package com.weibo.api.motan.filter.opentracing.zipkin.demo;

import io.opentracing.Tracer;
import io.opentracing.impl.BraveTracer;

import com.github.kristofa.brave.Brave;
import com.github.kristofa.brave.EmptySpanCollectorMetricsHandler;
import com.github.kristofa.brave.http.HttpSpanCollector;
import com.weibo.api.motan.filter.opentracing.TracerFactory;


/**
 * Created by Administrator on 2017/3/16 0016.
 */
public class MyTracerFactory implements TracerFactory {
    // any tracer implementation
   

    HttpSpanCollector.Config spanConfig = HttpSpanCollector.Config.builder().compressionEnabled(false)//默认false，span在transport之前是否会被gzipped。
            .connectTimeout(5000)//5s，默认10s
            .flushInterval(1)//1s
            .readTimeout(6000)//5s，默认60s
            .build();
    final Tracer braveTracer = new BraveTracer((new Brave.Builder("zscat"))
            .spanCollector(HttpSpanCollector
                    .create("http://127.0.0.1:9411", spanConfig,new EmptySpanCollectorMetricsHandler())));

    @SuppressWarnings("deprecation")
    @Override
    public Tracer getTracer() {
    	return braveTracer;
    }

}
