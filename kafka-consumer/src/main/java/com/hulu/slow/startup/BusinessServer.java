package com.hulu.slow.startup;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hulu.slow.kafka.KafkaConsumer;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by likaige on 2016-05-25.
 */
@Named
public class BusinessServer {

    private static Logger log = LoggerFactory.getLogger(BusinessServer.class);

    @Inject
    private static KafkaConsumer kafkaConsumer;

    @PostConstruct
    public void start() throws Exception {
        try {
            kafkaConsumer.start();
            log.error("BusinessServer start success.");
        } catch (Exception e) {
            log.error("BusinessServer start error! msg={}", e);
            System.exit(-1);
        }

    }

    public static void main(String[] args) {
    	 kafkaConsumer.start();
	}
}
